#!/bin/bash
assert(){ [[ ${?} -eq 0 ]] || { [[ -n ${1} ]] && { echo ${@} ; }; exit 1 ; } }

# Script para cambiar dominio en la base de datos
# Verifica el valor de la variable ENVIRONMENT y setea la URL correspondiente

# 0. verificar variables
[[ -n ${DB_NAME:+a} ]]
assert ERROR: falta la variable de entorno DB_NAME
[[ -n ${DB_USER:+a} ]]
assert ERROR: falta la variable de entorno DB_USER
[[ -n ${DB_PASSWORD:+a} ]]
assert ERROR: falta la variable de entorno DB_PASSWORD

# 1. esperar que la base levante
C=0
until mysql -h db -u ${DB_USER} -p${DB_PASSWORD} -e exit ${DB_NAME}
do
    [[ ${C} -lt 100 ]]
    assert ERROR: timeout esperando que levante la base de datos

    sleep 2
    C=$((${C} + 1))
done

mysqldump -h db -u ${DB_USER} -p${DB_PASSWORD} -r /dump/dump.sql ${DB_NAME};
chown $(stat -c %u:%g /dump) /dump/dump.sql;

# 4. imprimir mensaje de todo ok
cat - <<EOF



=====================================================================
=====================================================================

LISTO! DUMP EFECTUADO EN dump/dump.sql.

=====================================================================
=====================================================================



EOF
