#!/usr/bin/php -q
<?php

// php 5.3 date timezone requirement, shouldn't affect anything
date_default_timezone_set( 'America/Argentina/Cordoba' );

$opts = array(
	'h:' => 'host:',
	'n:' => 'name:',
	'u:' => 'user:',
	'p:' => 'pass:',
	'port:',
	'help'
);

$required = array(
	'h:',
	'n:',
	'u:',
	'p:'
);

function strip_colons( $string ) {
	return str_replace( ':', '', $string );
}

// store arg values
$arg_count 	= $_SERVER[ 'argc' ];
$args_array = $_SERVER[ 'argv' ];

$short_opts = array_keys( $opts );
$short_opts_normal = array_map( 'strip_colons', $short_opts );

$long_opts = array_values( $opts );
$long_opts_normal = array_map( 'strip_colons', $long_opts );

// store array of options and values
$options = getopt( implode( '', $short_opts ), $long_opts );

if ( isset( $options[ 'help' ] ) ) {
	echo "
Esperar conexion DB

ARGS
  -h, --host
    Required. The hostname of the database server.
  -n, --name
    Required. Database name.
  -u, --user
    Required. Database user.
  -p, --pass
    Required. Database user's password.
  --port
    Optional. Port on database server to connect to.
    The default is 3306. (MySQL default port).
  --help
    Displays this help message ;)
";
	exit;
}

// missing field flag, show all missing instead of 1 at a time
$missing_arg = false;

// check required args are passed
foreach( $required as $key ) {
	$short_opt = strip_colons( $key );
	$long_opt = strip_colons( $opts[ $key ] );
	if ( ! isset( $options[ $short_opt ] ) && ! isset( $options[ $long_opt ] ) ) {
		fwrite( STDERR, "Error: Missing argument, -{$short_opt} or --{$long_opt} is required.\n" );
		$missing_arg = true;
	}
}

// bail if requirements not met
if ( $missing_arg ) {
	fwrite( STDERR, "Please enter the missing arguments.\n" );
	exit( 1 );
}

// new args array
$args = array(
	'verbose' => true,
	'dry_run' => false
);

// create $args array
foreach( $options as $key => $value ) {

	// transpose keys
	if ( ( $is_short = array_search( $key, $short_opts_normal ) ) !== false )
		$key = $long_opts_normal[ $is_short ];

	// true/false string mapping
	if ( is_string( $value ) && in_array( $value, array( 'false', 'no', '0' ) ) )
		$value = false;
	if ( is_string( $value ) && in_array( $value, array( 'true', 'yes', '1' ) ) )
		$value = true;

	// boolean options as is, eg. a no value arg should be set true
	if ( in_array( $key, $long_opts ) )
		$value = true;

	// change to underscores
	$key = str_replace( '-', '_', $key );

	$args[ $key ] = $value;
}

print($args);

function wait_for_db( $args ) {

    $count = 0;
    do {
        $my = new mysqli($args['host'], $args['user'], $args['pass'], $args['name']);
        if ($my->connect_errno == 0) {
            print("Connection to database OK!\n");
            exit(0);
        }
        $count = $count + 1;
        sleep(2);
    } while ($count < 100);

    print("ERROR: Timed out waiting for connection.\n");
    exit(1);
}

wait_for_db($args);
