#!/bin/bash
assert(){ [[ ${?} -eq 0 ]] || { [[ -n ${1} ]] && { echo ${@} ; }; exit 1 ; } }

# Script para cambiar dominio en la base de datos
# Verifica el valor de la variable ENVIRONMENT y setea la URL correspondiente

# 0. verificar variables
[[ -n ${DB_NAME:+a} ]]
assert ERROR: falta la variable de entorno DB_NAME
[[ -n ${DB_USER:+a} ]]
assert ERROR: falta la variable de entorno DB_USER
[[ -n ${DB_PASSWORD:+a} ]]
assert ERROR: falta la variable de entorno DB_PASSWORD

# 1. esperar que la base levante
php wait-for-db.php -h db -n ${DB_NAME} -u ${DB_USER} -p ${DB_PASSWORD}
assert ERROR: timeout esperando que levante la base de datos

# 2. determinar cuales URLs deben ser reemplazadas
if [[ ${ENVIRONMENT,,} == 'dev' ]]
then
    SEARCH_URLS="${URL_STAGING} ${URL_PRODUCTION}"
    REPLACE_URL="${URL_DEV}"

    [[ -n ${URL_DEV:+a} ]]
    assert ERROR: falta setear variable URL_DEV
    [[ -n ${URL_STAGING:+a}${URL_PRODUCTION:+a} ]]
    assert ERROR: debe setear URL_STAGING y/o URL_PRODUCTION

elif [[ ${ENVIRONMENT,,} == 'staging' ]]
then
    SEARCH_URLS="${URL_DEV} ${URL_PRODUCTION}"
    REPLACE_URL="${URL_STAGING}"

    [[ -n ${URL_STAGING:+a} ]]
    assert ERROR: falta setear variable URL_STAGING
    [[ -n ${URL_DEV:+a}${URL_PRODUCTION:+a} ]]
    assert ERROR: debe setear URL_DEV y/o URL_PRODUCTION

elif [[ ${ENVIRONMENT,,} == 'production' ]]
then
    SEARCH_URLS="${URL_STAGING} ${URL_DEV}"
    REPLACE_URL="${URL_PRODUCTION}"

    [[ -n ${URL_PRODUCTION:+a} ]]
    assert ERROR: falta setear variable URL_PRODUCTION
    [[ -n ${URL_STAGING:+a}${URL_DEV:+a} ]]
    assert ERROR: debe setear URL_STAGING y/o URL_DEV

else
    echo ERROR: la variable ENVIRONMENT tiene valor desconocido: ${ENVIRONMENT}.
    exit 1
fi

# 3. reemplazar url
for URL in ${SEARCH_URLS}
do
    php srdb.cli.php -h db -n ${DB_NAME} -u ${DB_USER} -p ${DB_PASSWORD} \
	-s ${URL} -r ${REPLACE_URL}
    assert ERROR inesperado al cambiar dominio
done

# 4. imprimir mensaje de todo ok
cat - <<EOF



=====================================================================
=====================================================================

LISTO! DOMINIO CAMBIADO. PRESIONAR CONTROL-C PARA SALIR

=====================================================================
=====================================================================



EOF
