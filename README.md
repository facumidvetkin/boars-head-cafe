
# Template Wordpress

Repositorio template para inicializar una instancia Wordpress.

# Requerimientos

* Tener [Docker compose](dockercompose) instalado en la máquina local.
* Servicio Traefik corriendo en la máquina local. Ver
  [traefik](traefik) para las instrucciones sobre cómo configurarlo.

[traefik]: https://bitbucket.org/4rsoluciones/traefik/
[dockercompose]: https://docs.docker.com/compose/install/

# Modo de uso

## Crear repo nuevo y cargar contenido del template

1.  En Bitbucket, crear un repo vacío para el proyecto
    (`NOMBRE-PROYECTO`)

2.  En la máquina local, clonar este repo vacío:

        git clone https://bitbucket.org/4rsoluciones/NOMBRE-PROYECTO.git

3.  Dentro de la carpeta `NOMBRE-PROYECTO` agregar el contenido de este template
    y subirlo al repo recién creado :

        git remote add template https://bitbucket.org/4rsoluciones/template-wordpress.git
        git pull template master
        git push

## Inicialización de WordPress

1.  Editar el archivo `.env`. Setear correctamente las variables:

    * `WORDPRESS_VERSION`: Versión de WordPress a instalar. Por
      defecto se usa la última versión de https://hub.docker.com/_/wordpress.
    * `MYSQL_VERSION`: Versión de mysql a instalar. Por defecto se usa 5.7.
    * `URL_DEV`: URL local de desarrollo. Usar http://NOMBRE-PROYECTO.lndo.site.
    * `URL_STAGING`: URL de staging. Usar https://NOMBRE-PROYECTO.st4ging.dev.
    * `URL_PRODUCTION`: URL de producción.

2.  Ejecutar instalación de wordpress:

        docker-compose up -d

    Esto debería llenar la carpeta `src/` con la instalación de wordpress.

3.  Visitar http://NOMBRE-PROYECTO.lndo.site y completar la instalación de
    WordPress. Usar http://NOMBRE-PROYECTO.lndo.site como URL del sitio.

4.  Una vez finalizada la instalación, la carpeta `src/` contiene todos
    los archivos de instalación de WordPress. De ser necesario, cambiar
    el dueño de los archivos ejecutando (en Linux/macOS) `sudo chown -R
    USUARIO src/`.

5.  Trabajar normalmente sobre WordPress instalando temas, plugins,
    etc. Agregar a Git todos los cambios en `src/` que sean
    necesarios.

6.  Cuando está listo para publicar la versión, generar un dump de la
    base de datos (wordpress debe estar corriendo):

        docker-compose -f docker-compode.dump.yml up'

7.  Agregar el dump a Git:

        git add src/dump.sql

8.  Configurar archivos necesarios:

    * `.gitignore`: agregar carpetas a ignorar, por ejemplo la carpeta
      `dist` del tema.
    * `.dockerignore`: agregar node_modules, carpeta gulp, etc.
    * `docker-compose.theme.yml`: corregir path del tema y secuencia de
      comandos para compilar.
    * `bitbucket-pipelines.yml`: buscar los comentarios FIXME y
      corregir paths, comandos según indicaciones

9.  Commitear y publicar los cambios:

        git commit -m "Configuracion inicial WordPress"
        git push

## Trabajar con este proyecto

1.  Levantar la aplicacion:

        docker-compose up -d

2.  Bajar la aplicacion:

        docker-compose down [-v]

    La opción -v también borra la base de datos (útil para actualizarla
    desde un dump).

3.  (Re-)Compilar el tema:

        docker-compose -f docker-compose.theme.yml up

4.  Generar un dump de la base de datos:

        docker-compose up -d
        docker-compose -f docker-compose.dump.yml up

5.  Arreglar el dominio de la base de datos:

        docker-compose up -d
        docker-compose -f docker-compose.dbfix.yml up

    Se reemplazan las URLs configuradas en .env poniendo la del entorno
    correcto.

6.  Backup de la instalación (en staging/producción):

        docker-compose up -d
        docker-compose -f docker-compose.backup.yml up

    Genera un archivo backup.tar.gz con la instalacion completa de
    wordpress.

## Problemas

### El entorno staging no redirecciona correctamente

Algunos sitios generan un loop infinito ya que requieren HTTPS, pero
el servidor apache interno sirve contenido vía HTTP. Un workaround
para este problema es agregar el siguiente bloque al principio de
`src/wp-config.php`:

    if (isset($_SERVER["HTTP_X_FORWARDED_PROTO"] ) && "https" == $_SERVER["HTTP_X_FORWARDED_PROTO"] ) {
        $_SERVER["HTTPS"] = "on";
    }

Este código istruye a PHP a que reconozca el encabezado
X-Forwarded-Proto, seteado por el servidor frontend Traefik.
Referencia: https://www.wpcover.com/fixing-wordpress-infinite-redirect-https/

### Certificado inválido en staging

Esta situación puede ocurrir en dos casos:

1.  Es la primerísima vez que se accede al sitio.

	A veces, el frontend no llega a generar un certificado cuando se
    le solicita un nuevo dominio y retorna un certificado autofirmado.

	Para solucionar este problema, esperar unos segundos y recargar.

2.  Se está ingresando mal el dominio.

	El frontend genera certificados sólo para los servicios que
	conoce.  Si se ingresa un dominio desconocido, atiende con un
	certificado autofirmado.

	Para solucionar este problema, corregir la URL e intentar de
    nuevo. Se pueden chequear los dominios conocidos en
    https://traefik.st4ging.dev/dashboard/#/http/routers .

## Uso de la pipeline

La pipeline se configura en el archivo `bitbucket-pipelines.yml`.  La
pipeline contiene principalmente `steps` y `pipelines`. Cada `step` es
una funcionalidad concreta, por ejemplo hacer deploy en staging. Cada
`pipeline` ejecuta uno o más `steps`, y puede ser de ejecución
automática (por ejemplo `default`) o manual (las que se encuentran
dentro de `custom`).

Los `steps` configurados por defecto son:

-   `sonarqube-scan`: ejecuta el scanner sonarqube
-   `sonarqube-init`: inicializa el proyecto en sonarqube
-   `quality-gate`: ejecuta el quality gate de sonarqube
-   `theme`: compila el tema (usando imagen node)
-   `theme-docker`: compila el tema (ejecutando el archivo compose)
-   `docker`: construye imagenes docker para staging/producción
-   `package`: genera un paquete .tar.gz con la instalacion y los datos
    de la base
-   `deploy-staging`: hacer deploy de la aplicacion en staging
-   `undeploy-staging`: bajar la aplicacion y borrar datos de la misma
    en staging
-   `backup-staging`: hacer backup de la instalacion y la base de datos
    de staging
-   `cleanup-staging`: Borrar basura que va quedando en staging

Las `pipelines`:

-   `default`: (automática) se ejecuta con cada push
    - step: sonarqube-scan
    - step: quality-gate
    - step: theme
    - step: docker
-   `custom:A-deploy-staging`: deploya aplicacion en staging
    - step: deploy-staging
-   `custom:B-undeploy-staging`: Apaga y borra la aplicacion de staging
    - step: backup-staging
    - step: undeploy-staging
-   `custom:C-redeploy-staging`: Redeploya app desde cero en staging
    - step: backup-staging
    - step: undeploy-staging
    - step: deploy-staging
-   `custom:D-backup-staging`: Hace backup de staging
    - step: backup-staging
-   `custom:E-package`: Crear paquete de la aplicacion
    - step: theme
    - step: package
-   `custom:Y-sonarqube-init`: Inicializa proyecto en sonarqube
    - step: sonarqube-init
-   `custom:Z-cleanup-staging`: Ejecutar cada tanto para limpiar server staging
    - step: cleanup-staging

### Habilitar pipelines

Para usar las pipelines en Bitbucket, en la página del proyecto ir a
Settings > [Pipelines] Settings y activar la opción "Enable Pipelines"

### Conexión a entorno staging

Para poder ejecutar cualquier pipeline que trabaje sobre staging, se
debe configurar antes la conexión SSH. Para ello, en la página del
proyecto de Bitbucket acceder a Settings > [Pipelines] SSH Keys.  En
esta página, clickear "Use my own keys" y pegar en los campos
correspondientes la claves que se encuentran en el servidor de
archivos compartidos Producción. La clave privada es un archivo sin
extensión llamado `deployer`.  La clave pública es el archivo
`deployer.pub`. En la sección "Known hosts", escribir el nombre de
host `st4ging.dev`, luego clickear "Fetch" y finalmente "Add Host".

Listo! ya se puede usar la pipeline para hacer deploy a staging.
