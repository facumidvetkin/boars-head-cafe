# Carpeta de dumps

Los dumps en esta carpeta sirven para inicializar una base de datos
nueva, por ejemplo al hacer redeploy en staging.

Los archivos *.sql y *.sql.gz se ejecutan en orden alfabético. Si
queremos evitar que un archivo se ejecute, debemos cambiarle la
extensión, por ejemplo a .sql.ignore.
