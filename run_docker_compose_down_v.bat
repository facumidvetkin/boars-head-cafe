@ECHO OFF 
:: Run docker-compose down -v
TITLE Run docker-compose down -v
ECHO ============================
ECHO ENTER docker-compose down -v
ECHO ============================
echo %cd%
ECHO ============================
ECHO RUN docker-compose down -v
ECHO ============================
docker-compose down -v