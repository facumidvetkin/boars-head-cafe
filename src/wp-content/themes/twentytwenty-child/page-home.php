<?php

/**
 * Template Name: Template Home
 */

get_header();
?>

<main id="site-content" role="main">

    <div class="section-inner">

        <h1>Home</h1>

        <h2><?php the_field('home_spa_title'); ?></h2>

        <h2><?php the_field('home_eng_title'); ?></h2>

    </div><!-- .section-inner -->

</main><!-- #site-content -->

<?php
get_footer();
